//
//  Covid.swift
//  SiCovid
//
//  Created by Muhamad Widi Aryanto on 19/04/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

struct Covid {
    let image: UIImage
    let title: String
    let subtitle: String
    let description: String
}
