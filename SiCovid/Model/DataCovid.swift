//
//  DataCovid.swift
//  SiCovid
//
//  Created by Muhamad Widi Aryanto on 19/04/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

var covid: [Covid] = [
    Covid(
        image: UIImage(named: "coronavirus-2")!,
        title: "Koronavirus",
        subtitle: "Koronavirus atau coronavirus adalah sekumpulan virus dari subfamili Orthocoronavirinae",
        description: "dalam keluarga Coronaviridae dan ordo Nidovirales. Kelompok virus ini yang dapat menyebabkan penyakit pada burung dan mamalia (termasuk manusia). Pada manusia, koronavirus menyebabkan infeksi saluran pernapasan yang umumnya ringan, seperti pilek, meskipun beberapa bentuk penyakit seperti SARS, MERS, dan COVID-19 sifatnya lebih mematikan. Manifestasi klinis yang muncul cukup beragam pada spesies lain: pada ayam, koronavirus menyebabkan penyakit saluran pernapasan atas, sedangkan pada sapi dan babi menyebabkan diare. Belum ada vaksin atau obat antivirus untuk mencegah atau mengobati infeksi koronavirus pada manusia."
    ),
    Covid(
        image: UIImage(named: "scan")!,
        title: "ODP Dan PDP",
        subtitle: "ODP (Orang Dalam Pemantauan). Menurut Direktur Utama Rumah Sakit Umum Pusat Persahabatan",
        description: "Rita Rogayah, ODP adalah orang dalam pemantauan. Biasanya memiliki gejala ringan seperti batuk, sakit tenggorokan, demam, tetapi tidak ada kontak erat dengan penderita positif. Orang dengan status ODP biasanya tidak perlu rawat inap di rumah sakit tetapi akan diminta untuk melakukan isolasi secara mandiri di rumah setidaknya selama 14 hari hingga kondisi membaik. Kemudian, PDP (Pasien Dalam Pengawasan). Rita Rogayah juga menjelaskan, berbeda dengan ODP, orang yang dinyatakan PDP akan menjalani proses observasi melalui proses cek laboratorium yang hasilnya akan dilaporkan kepada Badan Penelitian dan Pengembangan Kesehatan (Balitbangkes) Kemenkes RI."
    ),
    Covid(
        image: UIImage(named: "security")!,
        title: "Lockdown",
        subtitle: "Istilah lockdown ramai menjadi perbincangan setelah beberapa negara  melakukan lockdown",
        description: "untuk menghindari penyebaran virus Corona. Lockdown artinya sebuah negara seperti Italia melakukan pengawasan ketat di semua wilayah negara, mengunci masuk atau keluar dari suatu wilayah/daerah/negara untuk mencegah penularan virus corona COVID-19. Pengawasan ketat ini dilakukan dengan berbagai cara. Salah satu yang dilakukan Italia ini adalah menutup semua toko kecuali toko makanan dan apotek. Lockdown atau Karantina wilayah adalah penerapan karantina terhadap suatu daerah atau wilayah tertentu dalam rangka mencegah perpindahan orang, baik masuk maupun keluar wilayah tersebut, untuk tujuan tertentu yang mendesak."
    ),
    Covid(
        image: UIImage(named: "keep-distance")!,
        title: "Physical Distancing",
        subtitle: "Physical Distancing adalah cara atau imbuan yang dilakukan kepada masyarakat",
        description: "untuk menjauhi segala bentuk perkumpulan, menjaga jarak antar manusia, menghindari berbagai pertemuan yang melibatkan banyak orang. Jika Anda harus berada di sekitar orang, jaga jarak dengan orang lain sekitar 6 kaki (2 meter). Konsep social distancing saat ini juga telah dilakukan di Bandara Ngurah Rai Bali dengan harapan dapat meminimalisir pesebaran virus Corona. Presiden Joko Widodo menekankan physical distancing untuk penanganan dan pencegahan virus corona COVID-19 di Indonesia. Physical distancing bisa diterjemahkan dengan jaga jarak atau jaga jarak aman dan disiplin untuk melaksanakannya, demikian seperti dikutip dari situs web Sekretariat Kabinet."
    ),
    Covid(
        image: UIImage(named: "quarantine")!,
        title: "Karantina",
        subtitle: "Menurut Organisasi Kesehatan Dunia (WHO), karantina dapat direkomendasikan untuk individu",
        description: "yang diyakini telah terpapar penyakit menular seperti COVID-19, tetapi tidak bergejala. Selain memantau jika gejalanya berkembang, berada di karantina berarti seseorang yang mungkin terpapar tidak akan menularkan penyakit kepada orang lain, karena mereka tinggal di rumah. Karantina adalah sistem yang mencegah perpindahan orang dan barang selama periode waktu tertentu untuk mencegah penularan penyakit. Sistem karantina identik dengan pengasingan terhadap seseorang atau suatu benda yang akan memasuki suatu negara atau wilayah. Dalam masa pengasingan, biasanya di area atau di sekitar pelabuhan atau bandara, dilakukan observasi dan pemeriksaan kesehatan."
    ),
    Covid(
        image: UIImage(named: "stay-at-home")!,
        title: "Work From Home",
        subtitle: "Work From Home (WFH) Kebijakan work from home atau bekerja dari rumah dipilih oleh",
        description: "beberapa perusahaan hingga lembaga pemerintahan. Bekerja dari rumah dalam kondisi saat ini diyakini dapat meminimalisir penularan virus Corona. Kerja jarak jauh adalah model atau perjanjian kerja di mana karyawan memperoleh fleksibilitas bekerja dalam hal tempat dan waktu kerja dengan bantuan teknologi telekomunikasi. Dengan kata lain, kegiatan bepergian ke kantor atau tempat kerja digantikan dengan hubungan telekomunikasi. Dengan sistem ini, banyak karyawan yang pada akhirnya bekerja di rumah, sementara lainnya, yang lazim disebut pekerja nomaden (nomad workers) atau web commuters menggunakan teknologi komunikasi untuk bekerja dari kafe atau tempat lain yang nyaman bagi mereka."
    ),
    Covid(
        image: UIImage(named: "crowd")!,
        title: "Epidemi",
        subtitle: "Wabah adalah istilah umum untuk menyebut kejadian tersebarnya penyakit pada daerah yang",
        description: "luas dan pada banyak orang, maupun untuk menyebut penyakit yang menyebar tersebut. Wabah dipelajari dalam epidemiologi. Dalam epidemiologi, epidemi adalah penyakit yang timbul sebagai kasus baru pada suatu populasi tertentu manusia, dalam suatu periode waktu tertentu, dengan laju yang melampaui laju ekspektasi (dugaan), yang didasarkan pada pengalaman mutakhir. Dengan kata lain, epidemi adalah wabah yang terjadi secara lebih cepat daripada yang diduga. Jumlah kasus baru penyakit di dalam suatu populasi dalam periode waktu tertentu disebut incidence rate (laju timbulnya penyakit)."
    ),
    Covid(
        image: UIImage(named: "pandemic")!,
        title: "Pandemi",
        subtitle: "Pandemi berarti epidemi atau penyebaran penyakit tertentu yang tejadi secara global",
        description: "dibanyak negara di dunia. ABC News mewartakan, pandemi tidak ada kaitannya dengan seberapa serius penyakit, tetapi pandemi adalah label bagi penyakit yang telah menyebar luas ke seluruh dunia. Pandemi adalah epidemi penyakit yang menyebar di wilayah yang luas, misalnya beberapa benua, atau di seluruh dunia. Penyakit endemik yang meluas dengan jumlah orang yang terinfeksi yang stabil bukan merupakan pandemi. Kejadian pandemi flu pada umumnya mengecualikan kasus flu musiman. Sepanjang sejarah, sejumlah pandemi penyakit telah terjadi, seperti cacar (variola) dan tuberkulosis."
    ),
    Covid(
        image: UIImage(named: "hand-wash")!,
        title: "Antiseptik",
        subtitle: "Antiseptik, dilansir dari Healthline, merupakan zat yang dapat menghentikan atau",
        description: "memperlambat pertumbuhan mikroorganisme. Penggunaan antiseptik aman pada jaringan hidup seperti pada permukaan kulit atau membran mukosa. Tidak jarang, antiseptik juga digunakan untuk membunuh mikroorganisme di dalam tubuh. Antiseptik atau germisida adalah senyawa kimia yang digunakan untuk membunuh atau menghambat pertumbuhan mikroorganisme pada jaringan yang hidup seperti pada permukaan kulit dan membran mukosa. Antiseptik berbeda dengan antibiotik dan disinfektan, yaitu antibiotik digunakan untuk membunuh mikroorganisme di dalam tubuh, dan disinfektan digunakan untuk membunuh mikroorganisme pada benda mati."
    ),
    Covid(
        image: UIImage(named: "bleach")!,
        title: "Disinfektan",
        subtitle: "Cairan disinfektan Dilansir dari Pharma Guideline, merupakan zat kimia yang digunakan",
        description: "untuk membersihkan dan membunuh kuman pada benda tak hidup. Pada umumnya, disinfektan digunakan untuk mensterilkan benda-benda dari pertumbuhan kuman dan bakteri. Cairan disinfektan atau Disinfektan adalah bahan kimia yang digunakan untuk mencegah terjadinya infeksi atau pencemaran oleh jasad renik atau obat untuk membasmi kuman penyakit. Pengertian lain dari disinfektan adalah senyawa kimia yang bersifat toksik dan memiliki kemampuan membunuh mikroorganisme yang terpapar secara langsung oleh disinfektan. Disinfektan tidak memiliki daya penetrasi sehingga tidak mampu membunuh mikroorganisme yang terdapat di dalam celah atau cemaran mineral."
    ),
]
