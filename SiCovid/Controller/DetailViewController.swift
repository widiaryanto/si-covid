//
//  DetailViewController.swift
//  SiCovid
//
//  Created by Muhamad Widi Aryanto on 19/04/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    @IBOutlet weak var detailCovidImage: UIImageView!
    @IBOutlet weak var detailCovidTitle: UILabel!
    @IBOutlet weak var detailCovidSubtitle: UILabel!
    @IBOutlet weak var detailCovidDescription: UILabel!
    
    var covid: Covid?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationItem.title = "Detail"
        if let result = covid {
            detailCovidImage.image = result.image
            detailCovidTitle.text = result.title
            detailCovidSubtitle.text = result.subtitle
            detailCovidDescription.text = result.description
        }
    }
}
