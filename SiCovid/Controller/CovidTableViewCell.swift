//
//  CovidTableViewCell.swift
//  SiCovid
//
//  Created by Muhamad Widi Aryanto on 19/04/20.
//  Copyright © 2020 Muhamad Widi Aryanto. All rights reserved.
//

import UIKit

class CovidTableViewCell: UITableViewCell {
    @IBOutlet weak var imageCovid: UIImageView!
    @IBOutlet weak var titleCovid: UILabel!
    @IBOutlet weak var subtitleCovid: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
